import React, { Component } from 'react';
import Form from "../Components/Form";
import { API_CONFIG } from "../Api_Config";
import axios from "axios";
import Weather from '../Components/Weather';

class WeatherFinder extends Component{
    state={
        city:null,
        country:null,
        temperature:null,
        humidity:null,
        description:null,
        error:null
    }

    getWeather = (event) => {
        event.preventDefault();
        const city = event.target.elements.city.value;
        const country = event.target.elements.country.value;
        const url = API_CONFIG.getCompleteUrl(city,country);
        axios.get(url)
            .then(response => {
                const data = response.data;
                console.log(data)
                if(city && country)
                {
                    this.setState({
                        temperature: data.main.temp,
                        city: data.name,
                        country: data.sys.country,
                        humidity: data.main.humidity,
                        description: data.weather[0].description,
                    })
                }
            })
            .catch(err => this.setState({
                error : 'Please enter the correct value.'
            }))
    }

    render() {
        return(
            <React.Fragment>

                <Form getWeather={this.getWeather}>
                    <Weather
                        city={this.state.city}
                        country={this.state.country}
                        temperature={this.state.temperature}
                        humidity={this.state.humidity}
                        description={this.state.description}
                        error={this.state.error}
                    />
                </Form>
            </React.Fragment>
        );
    }
}



export default WeatherFinder;