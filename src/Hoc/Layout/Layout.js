import './Layout.css';
import React from "react";

const Layout = (props) => {
    return(
        <div className="wrapper">
            <div className="main">
                <div className='container'>
                    <div className='row'>
                        {props.children}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Layout;