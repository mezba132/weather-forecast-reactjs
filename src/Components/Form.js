import React from 'react';
import './Form.css'

const Form = props => (
    <div className="col-6 form-container">
        <form onSubmit={props.getWeather}>
            <input type='text' name='city' placeholder='City'/>
            <input type='text' name='country' placeholder='Country'/>
            <button>Find Location</button>
        </form>
        {props.children}
    </div>
)

export default Form;