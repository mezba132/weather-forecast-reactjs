import './App.css';
import Layout from "./Hoc/Layout/Layout";
import React from "react";
import Map from "./Components/Map";
import Weather from "./Container/WeatherForcast";

const App = () => {
    return (
        <Layout>
            <Map/>
            <Weather/>
        </Layout>
    )
}

export default App;
